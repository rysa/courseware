let head = ''
if(userData){
	head  = `<div class="head_box">
		<div class="fl ov">
			<div class="fl logo_box"><img class="logo_img" src="${userData.logoInfo.logo.file_path}" /></div>
			<div class="logo_text fl">${userData.logoInfo.shop_name}</div>
		</div>
		<div class="fr ov">
			<div class="user_text fl">${userData.data.nickName}，欢迎您！</div>
			<div class="sign_out fl" title="退出登录"></div>
		</div>
		<div class="cl"></div>
	</div>`
}

Vue.component('common-head',{
  template:head
});