function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return decodeURIComponent(r[2]); return null;
};

// http://lesson.miaoyimeishu.com
let $url = "http://admin.miaoyimeishu.com"

let loginUrl = "http://admin.miaoyimeishu.com/index.php?s=/api/user/getqr/wxapp_id/10002";

let userData = sessionStorage.getItem('userInfo');
if(userData && userData != ''){
	userData = JSON.parse(userData); 
}else{
	userData = null
}

//退出登录
$(document).on('click','.sign_out',function(){
	sessionStorage.clear();
	window.location.href = loginUrl
});

//全局请求方法
function $request(request){
  //设置token
  if (userData && userData.token) {
    if (!request.data) {
      request.data = {};
    }
		if(request.data && !request.data.token){
			request.data.token = userData.token;
		}
    
  };
  //设置contentType
  let contentType = request.contentType?request.contentType:"application/x-www-form-urlencoded";
	$.ajax({
		url:$url+request.url,
		type:request.method,
		data:request.data,
		contentType:contentType,
		success:(res)=>{
			if(res.code==1){
				request.success && request.success(res)
			}else{
				layer.msg(res.msg,{time: 2000, icon:2});
			}
		}
	});
};